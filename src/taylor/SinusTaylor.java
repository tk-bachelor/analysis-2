package taylor;

public class SinusTaylor {
	private static int n = 9;

	/**
	 * Error: 0.01
	 * Interval: R
	 * 
	 * @param radian
	 * @return sin(radian)
	 */
	public static double mySin(double radian) {
		radian = radian % (2*Math.PI);
		if( radian > Math.PI){
			radian = radian - 2*Math.PI;
		}
		
		double result = 0;
		for (int i = 1; i <= n; i += 2) {
			result += calcExponent(radian, i);
		}
		return result;
	}

	private static double calcExponent(double x, int k) {
		return (Math.pow(-1, (k - 1) / 2) * Math.pow(x, k) / factorial(k));
	}

	private static int getOdd(int i) {
		return 2 * i - 1;
	}

	private static long factorial(int k) {
		long val = 1;
		for (long i = 1; i <= k; i++) {
			val *= i;
		}
		return val;
	}

	public static void main(String[] args) {
		double res = mySin(Math.PI+1);
		print(res);
		
		// between -PI and PI
		res = mySin(Math.PI); // 0.0
		print(res);
		
		print(mySin(Math.PI / 2)); // 1.0
		
		print(mySin(Math.PI / 4)); // 0.70		
		print(mySin(3*Math.PI /4)); // 0.70
		
		print(mySin(-Math.PI /4)); //-0.70
		print(mySin(-3*Math.PI /4)); // -0.70
	}
	
	private static void print(double result){
		System.out.println(result);
	}
}
